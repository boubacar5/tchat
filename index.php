<?php
require_once 'Database.php';


if(isset($_POST['submit']) && isset($_POST['users'])){

    // requette pour l'identifiant des utilisateur
    $stmt = $_db->prepare ("SELECT id_users FROM users ");
    $stmt->bindParam(1, $_POST['users']);
    $stmt->execute();

    // Verification userss
    if($stmt->rowCount() == 1){
        // Récupérer le résultat de la requête
        $resultat = $stmt->fetch();


        // Session
        session_start();
        $_SESSION['id_users'] = $resultat['id_users'];
        header("Location: messagerie.php");
        exit();
    }else{
        
        echo "Nom d'utilisateur invalide.";
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <form method="POST" action="">
        <h2>Connexion</h2>
        <label for="users">Nom d'utilisateur:</label>
        <input type="text" id="users" name="users" placeholder="Username" required> <br><br>
        <button type="submit" name="submit">Se connecter</button>
        <a href="inscription.php"><h4 align=center>Inscrivez vous ici</h4></a>
    </form>
</body>
</html>
