<?php
include 'Database.php';


if (isset($_POST['submit'])) {
    $users = $_POST['users'];

    if(isset($users) && !empty($users)){
        $stmt = $_db->prepare("INSERT INTO users (users)");
        $stmt->bindParam(1, $users);
        $stmt->execute();
        
        echo 'Inscription réussie!';
        header("location:messagerie.php");
    }else{
        echo 'Entrez votre nouvel username!<br>';        
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="css/style.css">    
    <title>Inscription</title>
</head>
<body>
    <form method="POST" action="">
        <h2>Inscription</h2>
        <label for="users">Crée un nom d'utilisateur:</label>
        <input type="text" id="users" name="users" placeholder="Entrez votre nom d'utilisateur" > <br><br>
        <button type="submit" name="submit">S'inscrire</button>
    </form>
</body>
</html>
