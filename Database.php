<?php

final class Database {
    const HOST = "localhost";
    const USERS = "tchat";
    const PASSWORD = "tchat";
    const DBNAME = "tchat";

    private $_db;

    public function __construct() {
        try {
            $this->_db = new PDO("mysql:host=". self::HOST. ";dbname=". self::DBNAME, self::USERS, self::PASSWORD, array());
            // echo "Connexion database réussie<br>";
        } catch(PDOException $e) {
            echo "Erreur connexion database" ;
        }
    }

    public function getDb() {
        return $this->_db;
    }

    
}

$_db = new Database();
$_db = $_db->getDb();

// public function inscriptionUsers($users) {
//     $query = $this->_db->prepare("INSERT INTO users ");
//     $query->bindParam(':users', $users);
//     $query->execute();
// }

// public function connectionUsers($users) {
//     $query = $this->_db->prepare("SELECT * FROM users ");
//     $query->bindParam(':users', $users);
//     $query->execute();
    
// }
